package cn.ideathink.androidpedometerdemo.app;

import android.app.Application;
import cn.ideathink.sensorlib.pedometer.StepService;
import com.xdandroid.hellodaemon.DaemonEnv;

/**
 * Created by yuandl on 2016-10-18.
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DaemonEnv.initialize(
                this,
                StepService.class,
                DaemonEnv.DEFAULT_WAKE_UP_INTERVAL);
        DaemonEnv.startServiceMayBind(StepService.class);
    }
}
